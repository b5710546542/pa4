package readability;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * UI of WordCounter can read file and url.
 * @author Jidapar Jettananurak
 *
 */
public class WordCounterUI {

	private JPanel background;
	private JLabel fileURL;
	private JTextField input;
	private JButton browse;
	private JButton count;
	private JButton clear;
	private JTextArea information;
	private JScrollPane scroll;
	
	private File file;
	private WordCounter wordCounter;

	/**
	 * Constructor.
	 */
	public WordCounterUI(){
		initComponents();
	}

	/**
	 * Create item and event of UI.
	 */
	public void initComponents(){
		JFrame frame = new JFrame();
		JPanel pane = new JPanel();
		pane.setLayout( new BoxLayout(pane , BoxLayout.Y_AXIS) );
		background = new JPanel();
		background.setLayout( new FlowLayout() );

		fileURL = new JLabel("File or URL name:");
		input = new JTextField(10);
		browse = new JButton("Browse");
		count = new JButton("Count");
		clear = new JButton("Clear");

		information = new JTextArea(5,25);
		scroll = new JScrollPane(information);
		
		ActionListener browselistener  = new BrowseButton();
		browse.addActionListener(browselistener);
		
		ActionListener countButton = new CountButton();
		count.addActionListener(countButton);
		
		ActionListener clearlistener = new ClearButton();
		clear.addActionListener(clearlistener);
		
		background.add(fileURL);
		background.add(input);
		background.add(browse);
		background.add(count);
		background.add(clear);

		pane.add(background);
		pane.add(scroll);

		frame.add(pane);
		frame.setVisible(true);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * ActionListener of browse it can find and choose file from computer document.
	 * @author Jidapar Jettananurak
	 *
	 */
	class BrowseButton implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			JFileChooser choose = new JFileChooser();
			choose.showOpenDialog(background);
			file = choose.getSelectedFile();
			input.setText("file:"+file.getAbsolutePath());
		}
	}
	
	/**
	 * ActionListener of count it can read file and show information in area.
	 * @author Jidapar Jettananurak
	 *
	 */
	class CountButton implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			
			try {
				URL url = new URL(input.getText());
				wordCounter = new WordCounter();
				String readfile = input.getText();
				int wordcount = wordCounter.countWords( url );
				int syllables = wordCounter.getSyllablesCount( );
				int sentence = wordCounter.getSentenceCount();
				double index = wordCounter.getIndex();
				String readability = wordCounter.getReadability();
				information.setText(String.format("Filename:\t\t%s\nNumber of Syllables:\t\t%d\nNumber of Words:\t\t%d\nNumber of Sentences:\t\t%d\nFlesch Index:\t\t%.1f\nReadability:\t\t%s"
						, readfile , syllables,  wordcount  , sentence , index , readability));
			} catch (MalformedURLException e1) {
			}
		}
		
	}
	
	/**
	 * ActionListener of clear it can clear all date.
	 * @author Jidapar Jettananurak
	 *
	 */
	class ClearButton implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			input.setText("");
			information.setText("");
		}
		
	}
	
}


