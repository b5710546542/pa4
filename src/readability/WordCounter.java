package readability;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Scanner;

/**
 * Counting syllables, words, sentence and calculate grade of file.
 * @author Jidapar Jettananurak
 *
 */
public class WordCounter {
	
	State state;
	private int syllables , numdash , totalSyllables = 0 ;
	private int numWord , totalWord = 0;
	private int sentence = 0;
	
	/**
	 * Count syllables from URL.
	 * @param url that want to count
	 * @return number of syllables
	 */
	public int countWords(URL url) {
		
		InputStream in = null;
		try {
			in = url.openStream( );
		} catch (IOException e) {
			
		}
		return countWords(in);
		
	}
	
	/**
	 * Count word and sentence from inputStream by the one sentence must have more one word.
	 * @param instream inputStream that want to count
	 * @return number of word
	 */
	public int countWords(InputStream instream) {
		numWord = 0;
		numdash = 1;

		Scanner input = new Scanner(instream);
		while(input.hasNext())
		{
			this.totalSyllables += countSyllables( input.next() );
			if(syllables != 0) numWord++;
			
			if(state == END_STATE && numWord > 1){
				this.totalWord += numWord;
				this.sentence++;
				numWord = 0;	
			}
		}
		return totalWord;
	}
	
	/**
	 * Get total syllables from counting.
	 * @return total syllables
	 */
	public int getSyllablesCount(){
		return totalSyllables;
	}
	
	/**
	 * Get total sentence from counting.
	 * @return
	 */
	public int getSentenceCount(){
		return sentence;
	}

	/**
	 * Get total word from counting.
	 * @return
	 */
	public int getWordCount(){
		return totalWord;
	}
	
	/**
	 * Set state of char of word.
	 * @param newstate
	 */
	public void setState( State newstate ){
		if( newstate != state ) newstate.enterState();
		this.state = newstate;
	}
	
	/**
	 * Count syllables of word.
	 * @param word that want to count 
	 * @return number of syllables
	 */
	public int countSyllables( String word ){
		state = START_STATE;
		syllables = 0;
		
		word = word.replaceAll("['\"():,]", "");

		for(int i = 0 ; i < word.length() ; i++){
			state.handlerChar(word.charAt(i));
		}
		
		if(state == DASH_STATE) {
			syllables = 0;
		}
		else if(state == VOWEL_STATE){
			syllables++;
		}
		else if(state == E_FIRST && syllables == 0){
			syllables++;
		}
		
		return syllables;
	}

	/**
	 * Check vowel.
	 * @param c character of word
	 * @return true if it is vowel
	 */
	private boolean isVowel( char c ){
		return "AEIOUaeiou".indexOf(c) >= 0 ;
	}

	/**
	 * Check letter.
	 * @param c character of letter
	 * @return true if it is letter
	 */
	private boolean isLetter( char c ){
		return Character.isLetter(c);
	}
	
	/**
	 * Check 'e','E' are first of vowel.
	 * @param c character of word
	 * @return true if it is e of first vowel
	 */
	private boolean isE_First( char c ){
		return c == 'E' || c == 'e';
	}

	/**
	 * Check 'y','Y'.
	 * @param c character of word
	 * @return true if it is 'y','Y'
	 */
	private boolean isY( char c ){
		return c == 'Y' || c == 'y';
	}
	
	/**
	 * Check dash.
	 * @param c character of word
	 * @return true if it is dash
	 */
	private boolean isDash( char c ){
		return c == '-' ;
	}
	
	/**
	 * Check end of word for count sentence.
	 * @param c character of word
	 * @return true if it is dash
	 */
	private boolean isWordEnding( char c ){
		return c == '.' || c == ';' || c == '?' || c == '!';		
	}
	
	/**
	 * Start state of char of word.
	 */
	State START_STATE = new State(){
		public void enterState() {
			// Don't do any thing.
		}

		public void handlerChar(char c) {
			if( isDash(c) ) setState(NONWORD);
			else if(isE_First(c)) setState(E_FIRST);
			else if( isVowel(c) ) setState( VOWEL_STATE );
			else if( isLetter(c) ) setState( CONSONANT_STATE );
			else setState( NONWORD );
		}
		
	};
	
	/**
	 * End state of char of word.
	 */
	State END_STATE = new State(){
		
		public void enterState() {
			// Don't do any thing.
		}

		public void handlerChar(char c) {
			setState( NONWORD );
		}
		
	};
	
	/**
	 * E state of char of word.
	 */
	State E_FIRST = new State(){

		public void enterState() {
			// Don't do any thing.
		}

		public void handlerChar(char c) {
			if( isWordEnding(c) ) setState(END_STATE);
			else if( isDash(c) ) setState(DASH_STATE);
			else if( isVowel(c) ) setState(VOWEL_STATE);
			else if( isLetter(c) ){
				syllables++;
				setState(CONSONANT_STATE);
			}
			else setState(NONWORD);
		}
		
	};
	
	/**
	 * Nonword state of char of word.
	 */
	State NONWORD = new State(){
		public void enterState() {
			syllables = 0;
		}

		public void handlerChar(char c) {
			if(isWordEnding(c)) setState(END_STATE);
			else setState(NONWORD);
		}
		
	};
	
	/**
	 * Dash state of char of word.
	 */
	State DASH_STATE = new State(){

		public void enterState() {
			// Don't do any thing.
		}

		public void handlerChar(char c) {
			if( isDash(c) ){
				if(numdash == 1){
					numWord++;
					numdash = 0;
				}
			}
			else if( isY(c) ){
				numdash = 1;
				setState( VOWEL_STATE );
							}
			else if( isE_First(c) ){
				numdash = 1;
				setState( E_FIRST );
			}
			else if( isVowel(c) ){
				numdash = 1;
				setState( VOWEL_STATE );
			}
			else if( isLetter(c) ) setState( CONSONANT_STATE ) ;
			else setState( NONWORD );
		}
		
	};
	
	/**
	 * Consonant state of char of word.
	 */
	State CONSONANT_STATE = new State(){
		public void handlerChar( char c ) {
			if(isWordEnding(c)) setState(END_STATE);
			else if( isDash(c) ) setState(DASH_STATE);
			else if( isY(c) ) setState( VOWEL_STATE );
			else if( isE_First(c) ) setState( E_FIRST );
			else if( isVowel(c) ) setState( VOWEL_STATE );
			else if( isLetter(c) ) /*Stay in this state*/ ;	
			else setState( NONWORD );
		}

		public void enterState() {
			// Don't do any thing.
		}

	};
	
	/**
	 * Vowel state of char of word.
	 */
	State VOWEL_STATE = new State(){
		public void handlerChar( char c ){
			if( isWordEnding(c) ){
				syllables++;
				setState(END_STATE);
			}
			else if( isDash(c) ){
				syllables++;
				setState(DASH_STATE);
			}
			else if( isY(c) ) {
				syllables++;
				setState(CONSONANT_STATE);
			}
			else if( isVowel(c) ) ;
			else if( isLetter(c) ) {
				syllables++;
				setState( CONSONANT_STATE );
			}
			else setState( NONWORD );
		}
	
		public void enterState(){
			// Don't do any thing.
		}
	};
	
	/**
	 * Calculate formula for the flesch readability index.
	 * @return
	 */
	public double getIndex(){
		return 206.835 - 84.6*((this.getSyllablesCount()*1.0) / (this.getWordCount()*1.0)) - 1.015*((this.getWordCount()*1.0) / (this.getSentenceCount()*1.0) );
	}
	
	/**
	 * Calculate a level appropriate for their intended readers. 
	 * @return String of degree of readers.
	 */
	public String getReadability(){
		double index = this.getIndex();
		
		if(index < 0) return "Advanced degree graduate";
		else if(index >= 0 && index < 30) return "College graduate";
		else if(index >= 30 && index < 50) return "College student";
		else if(index >= 50 && index < 60) return "High school student";
		else if(index >= 60 && index < 65) return "9th grade student";
		else if(index >= 65 && index < 70) return "8th grade student";
		else if(index >= 70 && index < 80) return "7th grade student";
		else if(index >= 80 && index < 90) return "6th grade student";
		else if(index >= 90 && index < 100) return "5th grade student";
		else return "4th grade student (elementary school)";
	}
	
}

/**
 * Status of state.
 * @author Jidapar Jettananurak
 *
 */
interface State{

	public void enterState();
	public void handlerChar( char c );
	
}