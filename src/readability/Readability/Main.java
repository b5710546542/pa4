package readability.Readability;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;

import readability.WordCounter;
import readability.WordCounterUI;

/**
 * Main for run WordCounter.
 * @author Jidapar Jettananurak
 *
 */
public class Main {

	/**
	 * Main for run application can read from cmd or GUI.
	 * @param args it can use in cmd reading.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException
	{
		if(args.length > 0){
			URL url = new URL(args[0]);
			WordCounter wordCounter = new WordCounter();
			int wordcount = wordCounter.countWords( url );
			int syllables = wordCounter.getSyllablesCount( );
			int sentence = wordCounter.getSentenceCount();
			double index = wordCounter.getIndex();
			String readability = wordCounter.getReadability();
			System.out.printf("Filename:\t\t%s\nNumber of Syllables:\t\t%d\nNumber of Words:\t\t%d\nNumber of Sentences:\t\t%d\nFlesch Index:\t\t%.1f\nReadability:\t\t%s"
					,args[0] , syllables,  wordcount  , sentence , index , readability);
		}
		else 
		{
			WordCounterUI wcu = new WordCounterUI();
		}
	}
	
}
